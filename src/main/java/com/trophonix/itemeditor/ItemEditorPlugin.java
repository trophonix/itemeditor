package com.trophonix.itemeditor;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class ItemEditorPlugin extends JavaPlugin {

  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(ChatColor.WHITE + "Sorry, that command is for players.");
      return true;
    }
    Player player = (Player) sender;
    int slot = player.getInventory().getHeldItemSlot();
    ItemStack heldItem = player.getInventory().getItem(slot);
    if (heldItem == null || heldItem.getType() == Material.AIR) {
      player.sendMessage(ChatColor.DARK_AQUA + "Hold an item to edit!");
      return true;
    }
    player.sendMessage(ChatColor.AQUA + "Opening item editor...");
    new EditMenu(this, heldItem).open(player);
    player.getInventory().setItem(slot, null);
    return true;
  }

}
