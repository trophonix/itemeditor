package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.EditMenu;
import com.trophonix.itemeditor.ItemEditorPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import java.util.List;

public abstract class OpenListMenuOption<T> extends ItemOption {

  private final List<ItemOption> menuOptions;
  private final boolean exitOnClick;

  public OpenListMenuOption(boolean exitOnClick, Material icon, String displayName, List<ItemOption> menuOptions) {
    super(icon, displayName);
    this.menuOptions = menuOptions;
    this.exitOnClick = exitOnClick;
  }

  public OpenListMenuOption(Material icon, String displayName, List<ItemOption> menuOptions) {
    this(false, icon, displayName, menuOptions);
  }

  @Override public void onClick(ItemEditorPlugin pl, Player player, EditMenu menu) {
    menu.setSaveOnClose(false);
    player.closeInventory();
    Bukkit.getScheduler().runTaskLater(pl, () -> {
      Inventory inv = Bukkit.createInventory(null, 9 * (int) Math.ceil((double) menuOptions.size() / 9),
          ChatColor.translateAlternateColorCodes('&', displayName));
      for (int i = 0; i < menuOptions.size(); i++) {
        inv.addItem(menuOptions.get(i).getIcon(menu.getItem()).build());
      }
      pl.getServer().getPluginManager().registerEvents(new Listener() {
        private boolean saveOnClose = true;

        @EventHandler
        public void onClick(InventoryClickEvent event) {
          if (inv.getViewers().contains(event.getWhoClicked())) {
            event.setCancelled(true);
            if (event.getSlot() < 0 || event.getRawSlot() > event.getView().getTopInventory().getSize()) return;
            ItemOption option = menuOptions.get(event.getSlot());
            saveOnClose = false;
            option.onClick(pl, (Player) event.getWhoClicked(), menu);
            inv.setItem(event.getSlot(), option.getIcon(menu.getItem()).build());
            saveOnClose = true;
            if (exitOnClick) {
              event.getWhoClicked().closeInventory();
            }
          }
        }
        @EventHandler
        public void onClose(InventoryCloseEvent event) {
          if (inv.getViewers().contains(event.getPlayer())) {
            HandlerList.unregisterAll(this);
            if (saveOnClose) {
              Bukkit.getScheduler().runTaskLater(pl, () -> {
                menu.open((Player) event.getPlayer());
                menu.setSaveOnClose(true);
                menu.updateOptionButtons();
                menu.updateItem();
              }, 1L);
            }
          }
        }
      }, pl);
      player.openInventory(inv);
    }, 1L);
  }

}
