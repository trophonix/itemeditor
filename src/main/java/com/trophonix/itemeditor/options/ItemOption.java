package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.EditMenu;
import com.trophonix.itemeditor.ItemBuilder;
import com.trophonix.itemeditor.ItemEditorPlugin;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public abstract class ItemOption {

  public final Material icon;
  public final String displayName;

  public ItemOption(Material icon, String displayName) {
    this.icon = icon;
    this.displayName = "&b" + displayName;
  }

  public abstract void onClick(ItemEditorPlugin pl, Player player, EditMenu menu);

  public ItemBuilder getIcon(ItemBuilder context) {
    return new ItemBuilder(icon).displayName(displayName);
  }

}
