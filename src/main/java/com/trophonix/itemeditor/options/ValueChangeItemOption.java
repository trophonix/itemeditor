package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.ItemBuilder;
import org.bukkit.Material;

import java.util.function.BiConsumer;
import java.util.function.Function;

public abstract class ValueChangeItemOption<T> extends ItemOption {

  public final Function<ItemBuilder, T> get;
  public final BiConsumer<ItemBuilder, T> set;

  public ValueChangeItemOption(Material icon, String displayName, Function<ItemBuilder, T> get,
                               BiConsumer<ItemBuilder, T> set) {
    super(icon, displayName);
    this.get = get;
    this.set = set;
  }

}
