package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.ItemBuilder;
import org.bukkit.Material;

import java.util.function.BiConsumer;
import java.util.function.Function;

public class IntConversationOption extends ConversationOption<Integer> {

  public IntConversationOption(Material icon, String displayName, Function<ItemBuilder, Integer> get,
                               BiConsumer<ItemBuilder, Integer> set, String internalName) {
    super(icon, displayName, get, set, internalName, in -> {
      try {
        return Integer.parseInt(in);
      } catch (NumberFormatException ignored) {
        throw new IllegalArgumentException("Invalid number '" + in + "'");
      }
    });
  }

}
