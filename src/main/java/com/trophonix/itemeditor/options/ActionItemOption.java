package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.EditMenu;
import com.trophonix.itemeditor.ItemBuilder;
import com.trophonix.itemeditor.ItemEditorPlugin;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;

import java.util.function.Consumer;

public class ActionItemOption extends ItemOption {

  private Consumer<ItemBuilder> action;

  public ActionItemOption(Material icon, String displayName, Consumer<ItemBuilder> action) {
    super(icon, displayName);
    this.action = action;
  }

  @Override public void onClick(ItemEditorPlugin pl, Player player, EditMenu menu) {
    action.accept(menu.getItem());
    menu.updateOptionButtons();
    menu.updateItem();
  }

  @Override public ItemBuilder getIcon(ItemBuilder context) {
    return new ItemBuilder(icon).displayName(displayName).flag(ItemFlag.HIDE_ATTRIBUTES).setLore(null);
  }
}
