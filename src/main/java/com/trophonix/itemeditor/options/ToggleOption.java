package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.EditMenu;
import com.trophonix.itemeditor.ItemBuilder;
import com.trophonix.itemeditor.ItemEditorPlugin;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;

import java.util.function.BiConsumer;
import java.util.function.Function;

public class ToggleOption extends ValueChangeItemOption<Boolean> {

  public ToggleOption(Material icon, String displayName, Function<ItemBuilder, Boolean> get, BiConsumer<ItemBuilder, Boolean> set) {
    super(icon, displayName, get, set);
  }

  @Override public void onClick(ItemEditorPlugin pl, Player player, EditMenu menu) {
    set.accept(menu.getItem(), !get.apply(menu.getItem()));
    menu.updateOptionButtons();
    menu.updateItem();
  }

  @Override public ItemBuilder getIcon(ItemBuilder context) {
    boolean b = get.apply(context);
    return new ItemBuilder(b ? Material.GREEN_WOOL : Material.RED_WOOL)
        .displayName(displayName).lore(" ", "&7Current Value: ", b ? "&aenabled" : "&cdisabled");
  }
}
