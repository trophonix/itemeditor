package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.EditMenu;
import com.trophonix.itemeditor.ItemBuilder;
import com.trophonix.itemeditor.ItemEditorPlugin;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.conversations.*;
import org.bukkit.entity.Player;

import java.util.function.BiConsumer;
import java.util.function.Function;

public class ConversationOption<T> extends ValueChangeItemOption<T> {

  private String prompt;
  private Function<String, T> parser;

  public ConversationOption(Material icon, String displayName, Function<ItemBuilder, T> get, BiConsumer<ItemBuilder,
                                                                                                           T> set,
                            Function<String, T> parser, String prompt) {
    super(icon, displayName, get, set);
    this.prompt = prompt;
    this.parser = parser;
  }

  public ConversationOption(Material icon, String displayName, Function<ItemBuilder, T> get, BiConsumer<ItemBuilder,
                                                                                                           T> set,
                            String internalName, Function<String, T> parser) {
    this(icon, displayName, get, set, parser, "Enter a new value for " + internalName + " ({CURRENT}" +
                                                  ChatColor.LIGHT_PURPLE + "):");
  }

  @Override public void onClick(ItemEditorPlugin pl, Player player, EditMenu menu) {
    menu.setSaveOnClose(false);
    player.closeInventory();
    new ConversationFactory(pl).withPrefix(new PluginNameConversationPrefix(pl, " > ", ChatColor.DARK_PURPLE)).withTimeout(120)
        .addConversationAbandonedListener(abandonedEvent -> {
          menu.open(player);
          menu.setSaveOnClose(true);
        }).withFirstPrompt(new ValidatingPrompt() {
      @Override protected boolean isInputValid(ConversationContext conversationContext, String s) {
        try {
          parser.apply(s);
          return true;
        } catch (IllegalArgumentException ignored) {
          return false;
        }
      }

      @Override protected Prompt acceptValidatedInput(ConversationContext conversationContext, String s) {
        set.accept(menu.getItem(), parser.apply(s));
        menu.updateOptionButtons();
        menu.updateItem();
        return null;
      }

      @Override public String getPromptText(ConversationContext conversationContext) {
        return ChatColor.LIGHT_PURPLE + prompt.replace("{CURRENT}", get.apply(menu.getItem()).toString());
      }
    }).buildConversation(player).begin();
  }

  @Override public ItemBuilder getIcon(ItemBuilder context) {
    T t = get.apply(context);
    return new ItemBuilder(icon).displayName(displayName)
               .lore(" ", "&7Current Value:", t != null ? "&f" + t.toString() : "&cN/A");
  }
}
