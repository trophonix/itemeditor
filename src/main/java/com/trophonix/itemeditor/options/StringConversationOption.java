package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.ItemBuilder;
import org.bukkit.Material;

import java.util.function.BiConsumer;
import java.util.function.Function;

public class StringConversationOption extends ConversationOption<String> {

  private static final Function<String, String> parser =
      in -> in.charAt(0) == '"' && in.charAt(in.length() - 1) == '"' ? in.substring(1, in.length() - 1) : in;

  public StringConversationOption(Material icon, String displayName, String internalName, Function<ItemBuilder,
                                                                                                      String> get,
                                  BiConsumer<ItemBuilder, String> set) {
    super(icon, displayName, get, set, internalName, parser);
  }

  public StringConversationOption(Material icon, String displayName, Function<ItemBuilder, String> get,
                                  BiConsumer<ItemBuilder, String> set, String prompt) {
    super(icon, displayName, get, set, parser, prompt);
  }

  @Override public ItemBuilder getIcon(ItemBuilder context) {
    String str = get.apply(context);
    return new ItemBuilder(icon).displayName(displayName).lore(" ", "&7Current Value:",
        str == null || str.isEmpty() ? "&cN/A" : "&f" + str);
  }
}
