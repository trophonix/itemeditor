package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.EditMenu;
import com.trophonix.itemeditor.ItemBuilder;
import com.trophonix.itemeditor.ItemEditorPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class OpenAddRemoveListMenuOption<T> extends ItemOption {

  private Function<ItemBuilder, List<ItemOption>> getOptions;
  private Function<ItemBuilder, List<T>> getList;
  private ItemOption addOption;
  private BiConsumer<ItemBuilder, Integer> remove;

  public OpenAddRemoveListMenuOption(Material icon, String displayName,
                                     Function<ItemBuilder, List<ItemOption>> getOptions, Function<ItemBuilder, List<T>> getList
      , ItemOption addOption, BiConsumer<ItemBuilder, Integer> remove) {
    super(icon, displayName);
    this.getOptions = getOptions;
    this.getList = getList;
    this.addOption = addOption;
    this.remove = remove;
  }

  @Override public void onClick(ItemEditorPlugin pl, Player player, EditMenu menu) {
    List<ItemOption> options = new LinkedList<>(this.getOptions.apply(menu.getItem()));
    int size = 9 * (int) Math.ceil((((double) options.size() + 1) / 9D));
    Inventory inv = Bukkit.createInventory(null, size, ChatColor.translateAlternateColorCodes('&', displayName + " &7(Click one to remove)"));
    updateOptions(menu, inv, options);
    pl.getServer().getPluginManager().registerEvents(new Listener() {
      private boolean saveOnClose = true;
      @EventHandler
      public void onClick(InventoryClickEvent event) {
        if (inv.getViewers().contains(event.getWhoClicked())) {
          event.setCancelled(true);
          if (event.getSlot() < 0 || event.getRawSlot() > event.getView().getTopInventory().getSize()) return;
          ItemOption option = options.get(event.getSlot());
          saveOnClose = false;
          System.out.println("Before click " + saveOnClose);
          option.onClick(pl, (Player) event.getWhoClicked(), menu);
          inv.setItem(event.getSlot(), option.getIcon(menu.getItem()).build());
          saveOnClose = true;
          System.out.println("After click " + saveOnClose);
        }
      }
      @EventHandler
      public void onClose(InventoryCloseEvent event) {
        System.out.println("onClose " + saveOnClose);
        if (inv.getViewers().contains(event.getPlayer())) {
          HandlerList.unregisterAll(this);
          if (saveOnClose) {
            Bukkit.getScheduler().runTaskLater(pl, () -> {
              menu.open((Player) event.getPlayer());
              System.out.println("Opening");
              menu.setSaveOnClose(true);
              menu.updateOptionButtons();
              menu.updateItem();
            }, 1L);
          }
        }
      }
    }, pl);
    menu.setSaveOnClose(false);
    player.closeInventory();
    player.openInventory(inv);
  }

  @Override public ItemBuilder getIcon(ItemBuilder context) {
    return super.getIcon(context).addLore(" ")
               .lore(getList.apply(context).stream().map(
                   obj -> ChatColor.translateAlternateColorCodes('&', "&7 - &f" + obj)
               ).toArray(String[]::new));
  }

  private void updateOptions(EditMenu menu, Inventory inv, List<ItemOption> options) {
    inv.clear();
    options.add(addOption);
    int slot = 0;
    for (ItemOption op : options) {
      inv.setItem(slot++, op.getIcon(menu.getItem()).build());
    }
  }

  private void updateOptions(EditMenu menu, Inventory inv) {
    List<ItemOption> options = new LinkedList<>(this.getOptions.apply(menu.getItem()));
    updateOptions(menu, inv, options);
  }

}
