package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.ItemBuilder;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class OpenEnchantsMenuOption extends OpenListMenuOption<Map.Entry<Enchantment, Integer>> {

  public OpenEnchantsMenuOption(Material icon, String displayName) {
    super(icon, displayName,
        Arrays.stream(Enchantment.values()).map(e -> new IntConversationOption(
            Material.ENCHANTED_BOOK, WordUtils.capitalizeFully(e.getKey().getKey().replace("_", " ")),
            b -> b.getLevel(e), (b, v) -> b.enchant(e, v), e.getKey().getKey() + " level"
        )).collect(Collectors.toList()));
  }

  @Override public ItemBuilder getIcon(ItemBuilder context) {
    ItemBuilder builder = new ItemBuilder(icon).displayName(displayName).addLore(" ");
    if (context.getEnchants() != null && context.getEnchants().size() > 0) {
      context.getEnchants().forEach((e, l) -> builder.addLore("&7" + WordUtils.capitalizeFully(e.getKey().getKey().replace("_", " ")) + ": &f" + l));
    } else {
      builder.addLore("&cN/A");
    }
    return builder;
  }

}
