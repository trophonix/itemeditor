package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.ItemBuilder;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;

import java.util.Arrays;
import java.util.stream.Collectors;

public class OpenItemFlagMenuOption extends OpenListMenuOption<ItemFlag> {

  public OpenItemFlagMenuOption(Material icon, String displayName) {
    super(icon, displayName,
        Arrays.stream(ItemFlag.values()).map(f -> new ToggleOption(
            Material.GREEN_WOOL, f.name().toLowerCase().replace("_", " "),
            b -> b.hasFlag(f), (b, v) -> b.flag(f))).collect(Collectors.toList()));
  }

  @Override public ItemBuilder getIcon(ItemBuilder context) {
    ItemBuilder builder = new ItemBuilder(icon).displayName(displayName);
    if (context.getFlags().isEmpty()) builder.lore(" ", "&cN/A");
    else {
      builder.addLore(" ");
      context.getFlags().forEach(flag -> builder.addLore("&7 - &f" + WordUtils.capitalizeFully(flag.name())));
    }
    return builder;
  }

}
