package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.ItemBuilder;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class OpenBannerColorMenuOption extends OpenListMenuOption<DyeColor> {

  private static final Map<Material, DyeColor> MATERIAL_TO_DYE_COLOR = new HashMap<>();
  private static final Map<DyeColor, Material> DYE_COLOR_TO_MATERIAL = new HashMap<>();
  private static final Map<DyeColor, ChatColor> DYE_COLOR_TO_CHAT_COLOR;

  static {
    matLoop:
    for (Material mat : Material.values()) {
      for (DyeColor color : DyeColor.values()) {
        if (mat.name().startsWith(color.name())) {
          MATERIAL_TO_DYE_COLOR.put(mat, color);
          DYE_COLOR_TO_MATERIAL.put(color, mat);
          continue matLoop;
        }
      }
    }
    DYE_COLOR_TO_CHAT_COLOR = new HashMap<DyeColor, ChatColor>() {{
      put(DyeColor.WHITE, ChatColor.WHITE);
      put(DyeColor.ORANGE, ChatColor.GOLD);
      put(DyeColor.MAGENTA, ChatColor.LIGHT_PURPLE);
      put(DyeColor.LIGHT_BLUE, ChatColor.AQUA);
      put(DyeColor.YELLOW, ChatColor.YELLOW);
      put(DyeColor.LIME, ChatColor.GREEN);
      put(DyeColor.PINK, ChatColor.LIGHT_PURPLE);
      put(DyeColor.GRAY, ChatColor.DARK_GRAY);
      put(DyeColor.LIGHT_GRAY, ChatColor.GRAY);
      put(DyeColor.CYAN, ChatColor.DARK_AQUA);
      put(DyeColor.PURPLE, ChatColor.DARK_PURPLE);
      put(DyeColor.BLUE, ChatColor.DARK_BLUE);
      put(DyeColor.BROWN, ChatColor.BLACK);
      put(DyeColor.GREEN, ChatColor.GREEN);
      put(DyeColor.RED, ChatColor.RED);
      put(DyeColor.BLACK, ChatColor.BLACK);
    }};
  }

  public OpenBannerColorMenuOption() {
    super(true, Material.BLACK_BANNER, "Banner Color",
        MATERIAL_TO_DYE_COLOR.values().stream().map(
            dyeColor -> new ActionItemOption(
                DYE_COLOR_TO_MATERIAL.get(dyeColor), WordUtils.capitalizeFully(dyeColor.name().replace("_", " ")),
                b -> b.type(DYE_COLOR_TO_MATERIAL.get(dyeColor))
            )).collect(Collectors.toList()));
  }

  @Override public ItemBuilder getIcon(ItemBuilder context) {
    DyeColor dyeColor = MATERIAL_TO_DYE_COLOR.get(context.getType());
    return new ItemBuilder(context.getType()).displayName(
        DYE_COLOR_TO_CHAT_COLOR.get(dyeColor) + WordUtils.capitalizeFully(dyeColor.name()));
  }
}
