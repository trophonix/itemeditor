package com.trophonix.itemeditor.options;

import com.trophonix.itemeditor.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;

import java.util.List;
import java.util.stream.Collectors;

public class AddLoreItemOption extends OpenAddRemoveListMenuOption<String> {

  public AddLoreItemOption() {
    super(Material.MAP, "Modify Lore",
        itemBuilder -> itemBuilder.getLore().stream().map(
            line -> new ActionItemOption(Material.STRING, line, b -> {}))
                           .collect(Collectors.toList()), ItemBuilder::getLore,
        new StringConversationOption(Material.GREEN_STAINED_GLASS_PANE, "&a&l[+]",
            b -> String.join("\n", b.getLore()), ItemBuilder::addLore,
            "Enter a new line of lore for your item:\n" +
                ChatColor.GRAY + "Can surround with \"quotes\" for leading/ending spaces.") {
          @Override
          public ItemBuilder getIcon(ItemBuilder context) {
            return new ItemBuilder(icon).displayName(displayName);
          }
        },
        ItemBuilder::removeLore);
  }

  @Override public ItemBuilder getIcon(ItemBuilder context) {
    ItemBuilder builder = new ItemBuilder(icon).displayName(displayName)
        .lore(" ", "&7Current Value:");
    List<String> lore = context.getLore();
    if (lore.isEmpty()) return builder.addLore("&cN/A");
    else return builder.lore(lore);
  }
}
