package com.trophonix.itemeditor;

import com.trophonix.itemeditor.options.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.Damageable;

import java.util.HashMap;
import java.util.Map;

public class EditMenu implements Listener {

  private static final int ITEM_SLOT = 10, EXIT_SLOT = 19;
  private static final ItemStack PLACEHOLDER =
      new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).displayName(" ").flag(ItemFlag.HIDE_ATTRIBUTES).build();

  private ItemEditorPlugin pl;

  private Map<Integer, MenuOptions> options = new HashMap<>();

  @Getter @Setter private boolean saveOnClose = true;

  @Getter private ItemBuilder item;
  private Inventory menu;

  public EditMenu(ItemEditorPlugin pl, ItemStack item) {
    this.pl = pl;
    this.item = new ItemBuilder(item);
    menu = Bukkit.createInventory(null, 36, "Item Editor");

    int width = 9, height = menu.getSize() / 9;
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {

        if (
            i == 0 || i == width - 1 || j == 0 || j == height - 1 // add border
                || (i == 2 && j == 1) // fill spot to the right of the item
                || (i == 2 && j == 2) // fill spot to right of exit
        ) {
          menu.setItem(((j) * 9) + i, PLACEHOLDER);
        }
      }
    }

    updateItem();
    menu.setItem(EXIT_SLOT, new ItemBuilder(Material.BARRIER).displayName("&cClose Menu").build());

    int slot = 0;
    ItemStack i;
    for (MenuOptions option : MenuOptions.values) {
      if (option.metaClass != null && !option.metaClass.isInstance(this.item.getMeta())) {
        continue;
      }
      while (((i = menu.getItem(slot)) != null && i.getType() != Material.AIR)) {
        slot++;
      }
      options.put(slot++, option);
    }

    updateOptionButtons();

    pl.getServer().getPluginManager().registerEvents(this, pl);
  }

  @EventHandler
  public void onClick(InventoryClickEvent event) {
    if (menu.getViewers().contains(event.getWhoClicked())) {
      event.setCancelled(true);
      if (event.getRawSlot() > event.getView().getTopInventory().getSize()) return;
      if (event.getSlot() == EXIT_SLOT) {
        event.getWhoClicked().closeInventory();
        return;
      }
      MenuOptions option = options.get(event.getSlot());
      if (option != null) {
        option.onClick(pl, (Player) event.getWhoClicked(), this);
      }
    }
  }

  @EventHandler
  public void onClose(InventoryCloseEvent event) {
    if (menu.getViewers().contains(event.getPlayer())) {
      Player player = (Player) event.getPlayer();
      if (saveOnClose) {
        if (player.getInventory().getItemInMainHand().getType() == Material.AIR) {
          player.getInventory().setItem(player.getInventory().getHeldItemSlot(), item.build());
        } else {
          player.getInventory().addItem(item.build()) // drop item if unable to be added
              .forEach((slot, leftoverItem) -> player.getWorld().dropItemNaturally(player.getLocation(), leftoverItem));
        }
        cancel();
      }
    }
  }

  public void open(Player player) {
    if (!menu.getViewers().contains(player)) player.openInventory(menu);
  }

  public void updateItem() {
    menu.setItem(ITEM_SLOT, item.build());
  }

  public void updateOptionButtons() {
    options.forEach((slot, option) -> {
      System.out.println(slot + " " + option.option.displayName);
      menu.setItem(slot, option.getIcon(item).build());
    });
  }

  public void cancel() {
    menu.clear();
    HandlerList.unregisterAll(this);
  }

  @AllArgsConstructor @RequiredArgsConstructor
  public enum MenuOptions {

    STACK_SIZE(new IntConversationOption(Material.CLOCK, "Set Stack Size", ItemBuilder::getAmount,
        ItemBuilder::amount, "stack size")),
    DISPLAY_NAME(new StringConversationOption(Material.NAME_TAG, "Display Name", ItemBuilder::getDisplayName,
        ItemBuilder::displayName, "display name")),
    ADD_LORE(new AddLoreItemOption()),
    CLEAR_LORE(new ActionItemOption(Material.PAPER, "Clear Item Lore", b -> b.setLore(null))),
    UNBREAKABLE(new ToggleOption(Material.BEDROCK, "Unbreakable", ItemBuilder::isUnbreakable,
        ItemBuilder::unbreakable)),

    // LISTS
    ENCHANTS(new OpenEnchantsMenuOption(Material.ENCHANTED_BOOK, "Enchantments")),
    ITEM_FLAGS(new OpenItemFlagMenuOption(Material.CYAN_BANNER, "Misc Item Flags")),

    // BANNER
    BANNER_COLOR(BannerMeta.class, new OpenBannerColorMenuOption()),

    // BOOKS
    AUTHOR(BookMeta.class, new StringConversationOption(Material.WRITABLE_BOOK, "Author", ItemBuilder::getAuthor,
        ItemBuilder::author, "author")),
    TITLE(BookMeta.class, new StringConversationOption(Material.NAME_TAG, "Title", ItemBuilder::getTitle,
        ItemBuilder::title, "title")),

    // DAMAGEABLE
    DAMAGE(Damageable.class, new IntConversationOption(Material.DAMAGED_ANVIL, "Damage the Item",
        ItemBuilder::getDamage, ItemBuilder::damage, "damage")),
    ;

    public static final MenuOptions[] values = values();

    public Class metaClass;
    public final ItemOption option;

    public void onClick(ItemEditorPlugin pl, Player player, EditMenu menu) {
      option.onClick(pl, player, menu);
    }

    public ItemBuilder getIcon(ItemBuilder context) {
      return option.getIcon(context);
    }

  }

}
