package com.trophonix.itemeditor;

import com.google.common.base.Preconditions;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

public class ItemBuilder {

  private ItemStack stack;
  @Getter private ItemMeta meta;

  public ItemBuilder(Material type, int amount) {
    Preconditions.checkState(type != null, "Invalid item type.");
    Preconditions.checkState(amount > 0, "Invalid amount %s. Can't have negative item amount.", amount);
    this.stack = new ItemStack(type, amount);
    this.meta = this.stack.getItemMeta();
  }

  public ItemBuilder(Material type) {
    this(type, 1);
  }

  public ItemBuilder(ItemStack item) {
    Preconditions.checkNotNull(item, "Item is null.");
    this.stack = item;
    this.meta = stack.getItemMeta();
  }

  public ItemStack build() {
    this.stack.setItemMeta(this.meta);
    return this.stack;
  }

  public ItemBuilder copy() {
    ItemStack copy = this.stack.clone();
    copy.setItemMeta(this.meta);
    return new ItemBuilder(stack);
  }

  public ItemBuilder type(Material type) {
    this.stack = new ItemStack(type, this.stack.getAmount());
    ItemMeta meta = this.stack.getItemMeta();
    assert meta != null;
    Class<? extends ItemMeta> originalClazz = this.meta.getClass();
    Class<? extends ItemMeta> newClazz = meta.getClass();
    if (originalClazz == newClazz) {
      this.stack.setItemMeta(this.meta);
      return this;
    }
    // Set fields into new kind of meta
    for (Field field : originalClazz.getDeclaredFields()) {
      if (Modifier.isStatic(field.getModifiers())) continue;
      boolean wasAccessible = field.isAccessible();
      field.setAccessible(true);
      try {
        Field newField = newClazz.getDeclaredField(field.getName());
        if (Modifier.isStatic(newField.getModifiers())) continue;
        boolean wasNewFieldAccessible = newField.isAccessible();
        newField.setAccessible(true);
        newField.set(meta, field.get(this.meta));
        newField.setAccessible(wasNewFieldAccessible);
      } catch (NoSuchFieldException | IllegalAccessException ignored) {
      } finally {
        field.setAccessible(wasAccessible);
      }
    }
    return this;
  }

  public Material getType() {
    return this.stack.getType();
  }

  public ItemBuilder amount(int amount) {
    this.stack.setAmount(amount);
    return this;
  }

  public int getAmount() {
    return this.stack.getAmount();
  }

  public ItemBuilder displayName(String displayName) {
    this.meta.setDisplayName(displayName == null ? null : ChatColor.translateAlternateColorCodes('&', displayName));
    return this;
  }

  public String getDisplayName() {
    return meta.getDisplayName();
  }

  public ItemBuilder lore(List<String> toAdd) {
    List<String> lore = this.meta.getLore();
    if (lore == null) lore = new ArrayList<>();
    lore.addAll(toAdd.stream().map(str -> ChatColor.translateAlternateColorCodes('&', str))
                    .collect(Collectors.toList()));
    this.meta.setLore(lore);
    return this;
  }

  public ItemBuilder lore(String... lore) {
    return this.lore(Arrays.asList(lore));
  }

  public ItemBuilder addLore(String line) {
    return this.lore(line);
  }

  public ItemBuilder removeLore(int index) {
    Preconditions.checkArgument(this.meta.hasLore() && index < this.meta.getLore().size(), "Something went wrong with" +
                                                                                               " lore.");
    List<String> lore = this.meta.getLore();
    lore.remove(index);
    this.meta.setLore(lore);
    return this;
  }

  public List<String> getLore() {
    return meta.hasLore() ? meta.getLore() : new LinkedList<>();
  }

  public ItemBuilder setLore(List<String> lore) {
    this.meta.setLore(lore);
    return this;
  }

  public ItemBuilder enchant(Enchantment enchant, int level) {
    this.meta.addEnchant(enchant, level, true);
    return this;
  }

  public ItemBuilder enchant(Enchantment enchant) {
    return this.enchant(enchant, 1);
  }

  public int getLevel(Enchantment enchant) {
    return this.meta.getEnchantLevel(enchant);
  }

  public Map<Enchantment, Integer> getEnchants() {
    return meta.getEnchants();
  }

  public List<Map.Entry<Enchantment, Integer>> getEnchantsAsEntries() {
    List<Map.Entry<Enchantment, Integer>> result = new ArrayList<>();
    for (Enchantment enchant : Enchantment.values()) {
      result.add(new AbstractMap.SimpleEntry<>(enchant, this.meta.getEnchantLevel(enchant)));
    }
    return result;
  }

  public ItemBuilder unbreakable(boolean unbreakable) {
    this.meta.setUnbreakable(unbreakable);
    return this;
  }

  public ItemBuilder unbreakable() {
    return this.unbreakable(true);
  }

  public boolean isUnbreakable() {
    return meta.isUnbreakable();
  }

  public ItemBuilder flag(ItemFlag flag) {
    if (this.meta.hasItemFlag(flag)) this.meta.removeItemFlags(flag);
    else this.meta.addItemFlags(flag);
    return this;
  }

  public boolean hasFlag(ItemFlag flag) {
    return this.meta.hasItemFlag(flag);
  }

  public List<ItemFlag> getFlags() {
    return new ArrayList<>(meta.getItemFlags());
  }

  //
  // Banner
  //
  public ItemBuilder addBannerLayer(Pattern pattern) {
    if (this.meta instanceof BannerMeta) {
      ((BannerMeta) this.meta).addPattern(pattern);
    }
    return this;
  }

  public Pattern getBannerLayer(int index) {
    if (this.meta instanceof BannerMeta) {
      return ((BannerMeta) this.meta).getPattern(index);
    }
    return null;
  }

  //
  // Books
  //
  public ItemBuilder author(String author) {
    if (this.meta instanceof BookMeta) {
      ((BookMeta) this.meta).setAuthor(author);
    }
    return this;
  }

  public String getAuthor() {
    if (this.meta instanceof BookMeta) {
      return ((BookMeta) this.meta).getAuthor();
    }
    return null;
  }

  public ItemBuilder title(String title) {
    if (this.meta instanceof BookMeta) {
      ((BookMeta) this.meta).setTitle(title);
    }
    return this;
  }

  public String getTitle() {
    if (this.meta instanceof BookMeta) {
      return ((BookMeta) this.meta).getTitle();
    }
    return null;
  }

  //
  // Damageable
  //
  public ItemBuilder damage(int damage) {
    if (this.meta instanceof Damageable) {
      ((Damageable) this.meta).setDamage(damage);
    }
    return this;
  }

  public int getDamage() {
    if (this.meta instanceof Damageable) {
      return ((Damageable) this.meta).getDamage();
    }
    return 0;
  }

}
